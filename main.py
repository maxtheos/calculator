from flask import Flask, redirect, url_for, render_template, request
import sympy
import math

eel.init("templates")
app = Flask(__name__)

def eelProcess():
    eel.start("", size=(1000,600))

def flaskProcess():
    app.run(host="localhost", port=8000, debug=True)

result = "0"
ans = "0"


@app.route("/", methods=["POST", "GET"])
def index():
    if request.method == "POST":
        return redirect(url_for("basic"))
    return render_template("index.html")


@app.route("/menu", methods=["POST", "GET"])
def menu():
    global result
    global ans
    if request.method == "POST":
        if request.form.get("one"):
            if result == "0":
                result = "1"
            else:
                result += "1"
        elif request.form.get("two"):
            if result == "0":
                result = "2"
            else:
                result += "2"
        elif request.form.get("three"):
            if result == "0":
                result = "3"
            else:
                result += "3"
        elif request.form.get("four"):
            if result == "0":
                result = "4"
            else:
                result += "4"
        elif request.form.get("five"):
            if result == "0":
                result = "5"
            else:
                result += "5"
        elif request.form.get("six"):
            if result == "0":
                result = "6"
            else:
                result += "6"
        elif request.form.get("seven"):
            if result == "0":
                result = "7"
            else:
                result += "7"
        elif request.form.get("eight"):
            if result == "0":
                result = "8"
            else:
                result += "8"
        elif request.form.get("nine"):
            if result == "0":
                result = "9"
            else:
                result += "9"
        elif request.form.get("zero"):
            if result != "0":
                result += "0"
        elif request.form.get("dot"):
            if "." not in result:
                result += "."
        elif request.form.get("ac"):
            result = "0"
        elif request.form.get("ans"):
            if result == "0":
                result = ans
            else:
                result += ans
        elif request.form.get("del"):
            if result != "0":
                if len(result) == 1:
                    result = "0"
                else:
                    result = result[:-1]
        elif request.form.get("divide"):
            if result != "0":
                if result[-1] != "*" and result[-1] != "/" and result[-1] != "-" and result[-1] != "+":
                    result += "/"
        elif request.form.get("multiply"):
            if result != "0":
                if result[-1] != "*" and result[-1] != "/" and result[-1] != "-" and result[-1] != "+":
                    result += "*"
        elif request.form.get("minus"):
            if result != "0":
                if result[-1] != "*" and result[-1] != "/" and result[-1] != "-" and result[-1] != "+":
                    result += "-"
        elif request.form.get("plus"):
            if result != "0":
                if result[-1] != "*" and result[-1] != "/" and result[-1] != "-" and result[-1] != "+":
                    result += "+"
        elif request.form.get("equal"):
            result = "x=" + result
            expression = result.replace("=","-(")+")"
            grouped = eval(expression.replace("x",'1j'))
            result =  str(round(-grouped.real/grouped.imag, 2))
            if result[-1] == "0" and result[-2] == ".":
                result = result[:-2]
            ans = str(int(round(-grouped.real/grouped.imag, 2)))
        elif request.form.get("cos"):
            result = str(math.cos(float(result)))
        elif request.form.get("sin"):
            result = str(math.sin(float(result)))
        elif request.form.get("tg"):
            result = str(math.tan(float(result)))
        elif request.form.get("ctg"):
            result = str(math.atan(float(result)))
        elif request.form.get("exp"):
            result = str(math.exp(float(result)))
        elif request.form.get("sqrt"):
            result = str(math.sqrt(float(result)))
        elif request.form.get("ln"):
            result = str(math.log(float(result)))
        elif request.form.get("lg"):
            result = str(math.log10(float(result)))
        elif request.form.get("abs"):
            result = str(math.fabs(float(result)))
        elif request.form.get("int"):
            result = str(int(result))
        elif request.form.get("invert"):
            result = str(pow(int(result), -1))
        elif request.form.get("rad"):
            result = str(math.radians(float(result)))
        elif request.form.get("sign"):
            if int(result) < 0:
                result = "-1"
            elif int(result) > 0:
                result = "1"
            else:
                result = "0"
        elif request.form.get("e"):
            result += str(math.e)
        elif request.form.get("pi"):
            result += str(math.pi)
    return render_template("menu.html", result=result)


@app.route("/basic", methods=["POST", "GET"])
def basic():
    global result
    global ans
    if request.method == "POST":
        if request.form.get("one"):
            if result == "0":
                result = "1"
            else:
                result += "1"
        elif request.form.get("two"):
            if result == "0":
                result = "2"
            else:
                result += "2"
        elif request.form.get("three"):
            if result == "0":
                result = "3"
            else:
                result += "3"
        elif request.form.get("four"):
            if result == "0":
                result = "4"
            else:
                result += "4"
        elif request.form.get("five"):
            if result == "0":
                result = "5"
            else:
                result += "5"
        elif request.form.get("six"):
            if result == "0":
                result = "6"
            else:
                result += "6"
        elif request.form.get("seven"):
            if result == "0":
                result = "7"
            else:
                result += "7"
        elif request.form.get("eight"):
            if result == "0":
                result = "8"
            else:
                result += "8"
        elif request.form.get("nine"):
            if result == "0":
                result = "9"
            else:
                result += "9"
        elif request.form.get("zero"):
            if result != "0":
                result += "0"
        elif request.form.get("dot"):
            if "." not in result:
                result += "."
        elif request.form.get("ac"):
            result = "0"
        elif request.form.get("ans"):
            if result == "0":
                result = ans
            else:
                result += ans
        elif request.form.get("del"):
            if result != "0":
                if len(result) == 1:
                    result = "0"
                else:
                    result = result[:-1]
        elif request.form.get("divide"):
            if result != "0":
                if result[-1] != "*" and result[-1] != "/" and result[-1] != "-" and result[-1] != "+":
                    result += "/"
        elif request.form.get("multiply"):
            if result != "0":
                if result[-1] != "*" and result[-1] != "/" and result[-1] != "-" and result[-1] != "+":
                    result += "*"
        elif request.form.get("minus"):
            if result != "0":
                if result[-1] != "*" and result[-1] != "/" and result[-1] != "-" and result[-1] != "+":
                    result += "-"
        elif request.form.get("plus"):
            if result != "0":
                if result[-1] != "*" and result[-1] != "/" and result[-1] != "-" and result[-1] != "+":
                    result += "+"
        elif request.form.get("equal"):
            result = "x=" + result
            expression = result.replace("=","-(")+")"
            grouped = eval(expression.replace("x",'1j'))
            result =  str(round(-grouped.real/grouped.imag, 2))
            if result[-1] == "0" and result[-2] == ".":
                result = result[:-2]
            ans = str(int(round(-grouped.real/grouped.imag, 2)))
        if request.form.get("menu"):
            return redirect(url_for("menu"))
    return render_template("basic.html", result=result)


if __name__ == "__main__":
    Thread(target=flaskProcess).run()
    Thread(target=eelProcess).start()
